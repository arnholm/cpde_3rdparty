@echo off
REM  This procedure uses cmake to build static wxWidgets libraries for Windows/MSVC
REM  See also https://docs.wxwidgets.org/latest/overview_cmake.html
REM  
REM  NOTE: Procedure must be run from "Visual Studio Command Prompt"
REM
mkdir ..\msvclibs
pushd ..\msvclibs
rem git clone --recurse-submodules --depth 1 --branch v3.0.4 https://github.com/wxWidgets/wxWidgets wx
git clone --recurse-submodules --depth 1 --branch v3.2.5 https://github.com/wxWidgets/wxWidgets wx
pushd wx
set BUILDDIR=.\build_cmake
REM Note that '^' must be the last character on each line
cmake -B "%BUILDDIR%" ^
   -DwxBUILD_INSTALL:BOOL="1" ^
   -DCMAKE_INSTALL_PREFIX:PATH=".." ^
   -DwxBUILD_SHARED:BOOL="0"  ^
   -DwxBUILD_MONOLITHIC:BOOL="0" 
pushd %BUILDDIR%
REM buld and install libraries under wx\lib
msbuild INSTALL.vcxproj   /p:Platform="x64" /p:Configuration=Release /m
msbuild INSTALL.vcxproj   /p:Platform="x64" /p:Configuration=Debug   /m
REM
REM return to the directory of this batch file
cd %~dp0
